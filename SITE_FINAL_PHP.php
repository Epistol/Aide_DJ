
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="bulma.css" />
    <link rel="stylesheet" href="site_final.css" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <title>Données météo à Rennes</title>
</head>

<body>
<header>
    <h1>Données météo à Rennes</h1>
    <h2>Site affilié à l'UMR - LETG de Rennes 2</h2>
</header>
<div class="container">
<p class="instructions"> <br/> Afin de choisir les données que vous voulez voir s'afficher, cochez ou décochez les différentes cases et veuillez sélectionner
    une plage de dates. <br/> <br/> </p>



<section> <!-- Partie avec les cases à cocher et autres formulaires -->
    <article>

        <form method="post" action="traitement.php">
            <!--  method = attribut indiquant par quel moyen les données vont être envoyées.
				post = méthode la + utilisée pour les formulaires (+ grand nb d'infos envoyées et données saisies ne transitent pas par l'URL)
				action = adresse de la page ou du programme qui va traiter les informations (généralement en PHP).
			-->
            <p>
            <div id="conteneur">  <!-- Pour organiser les parties avec flexbox dans le .css -->
                <div class="element"> <u><b> Valeurs : </b></u> </br>
                    <input type="checkbox" name="temperature" id="temperature" checked /> <label for="temperature">Température</label> <br />
                    <input type="checkbox" name="pression" id="pression" checked /> <label for="pression">Pression</label> <br/>
                    <input type="checkbox" name="hygrometrie" id="hygrometrie" checked /> <label for="hygrometrie">Hygrométrie</label> <br/> <br/>
                </div>
                <div class="element"> <u><b> Précisions : </b></u> </br>
                    <input type="checkbox" name="longitude" id="longitude" checked /> <label for="longitude">Longitude</label> <br />
                    <input type="checkbox" name="latitude" id="latitude" checked /> <label for="latitude">Latitude</label> <br/>
                    <input type="checkbox" name="adresse" id="adresse" checked /> <label for="adresse">Adresse</label> <br/> <br/>
                </div>
                <div class="element"> <u><b> Date et heure : </b></u> </br>
                    <input type="datetime-local" id="datepicker"/>
                </div>
               <u><b> Stations météo : </b></u> </br>
                <div>

            <p class="control">
    <span class="select">
      <select>


<?php

                    for ($i = 1; $i <= 6; $i++){
                        ?>
                       <!-- <input type="checkbox" name="station<?/*= $i;*/?>" id="stations" checked /> <label for="stations">Station <?/*= $i;*/?></label>-->
                        <option>Station <?= $i;?></option>
                    <?php
                    }
?>

      </select>
    </span>
            </p>
</div>


                <!-- L'attribut name permettra de recueillir la valeur du champ après validation du formulaire ; for et id doivent être identiques pour lier le label (for) au champ (id) -->
            </div>
            </p>
<button class="button is-primary" type="submit">Envoyer</button>


        </form> <!-- Fin du formulaire -->
    </article>
</section>



<section class="section">

	<?php include_once("siteFinal.php");?>

</section>


</div>

    <footer>
        <p>Site créé par Joaquim Delplace.<br />
            <a href="#">Me contacter !</a></p>
    </footer>
</body>
</html>

