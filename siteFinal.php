<table>
	<?php
	/* Paramètres de connexion à la BDD */
	$Host="localhost";
	$NameUser="root";
	$Password="";
	$NameDB="dj_help";
	$connex = mysqli_connect("$Host", "$NameUser", "$Password", "$NameDB");

	/* Vérification de la connexion */
	if (mysqli_connect_errno()) {
		printf("Échec de la connexion : %s\n", mysqli_connect_error());
		exit();
	}

	$requete = "SELECT pression, hygrometrie, temperature, latitude, longitude, adresse FROM mesures, station WHERE station.id_station = mesures.id_station";
	$result = mysqli_query($connex, $requete);  //mysqli_query exécute une requête sur la base de données

	// Test de la valeur de $result
	if($result == false){
		printf("Echec de la requête sur la base de données : %s\n", mysqli_connect_error());
		exit();
	}

	//$reponse = mysqli_result($result, MYSQLI_BOTH);

	?>			<table class="table">
        <thead>
        <tr>
            <!-- <th>ID</th> -->
            <th>Pression</th>
            <th>Hygrométrie</th>
            <th>Température</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th>Adresse</th>
        </tr>
        </thead>

		<?php
		while($ligne=mysqli_fetch_row($result))   //Récupère une ligne de résultat sous forme de tableau indexé
		{
			?>

            <tr>

	            <?php

	            if(isset($_GET['pression'])){
		            echo '  <td>'.$ligne[1].'hPa</td>';
	            }
	            else  if(isset($_GET['hygro'])){
		            echo '  <td>'.$ligne[1].'%</td>';
	            }
	            else  if(isset($_GET['temp'])){
		            echo '  <td>'.$ligne[2].'°C</td>';
	            }
	            else if(isset($_GET['lat'])){
		            echo '  <td>'.$ligne[3].'°</td>';
	            }
	            else if(isset($_GET['long'])){
		            echo '  <td>'.$ligne[4].'°</td>';
                }
	            else if(isset($_GET['adresse'])){
		            echo '  <td>'.$ligne[5].'°</td>';
	            }
	            else {


	            ?>

                <td><?php echo("$ligne[0] hPa");?></td>
                <td><?php echo("$ligne[1] %");?></td>
                <td><?php echo("$ligne[2]°C");?></td>
                <td><?php echo("$ligne[3]°");?></td>
                <td><?php echo("$ligne[4]°");?></td>
                <td><?php echo("$ligne[5]");?></td>
                <?php  }?>
            </tr>

			<?php
		}
		?>
    </table>



	<?php
	//Pour savoir le nombre de lignes retournée par le SELECT
	//echo mysqli_num_rows($result);

	$requeteZ = "SELECT MAX(temperature), MIN(temperature), AVG(temperature) AS temp_moyen FROM mesures";
	$resultZ = mysqli_query($connex, $requeteZ);  //mysqli_query exécute une requête sur la base de données
	$ligneZ = mysqli_fetch_row($resultZ);
	echo("+ haute température : $ligneZ[0] ; + basse température : $ligneZ[1] ; Moyenne températures : $ligneZ[2] \n");



	$requeteD = "SELECT pression, temperature, hygrometrie, date FROM mesures";
	$resultD = mysqli_query($connex, $requeteD);  //mysqli_query exécute une requête sur la base de données
	//$ligneD = mysqli_fetch_row($resultD);

	$requeteE = "SELECT id_station, latitude, longitude, adresse FROM station";
	$resultE = mysqli_query($connex, $requeteE);  //mysqli_query exécute une requête sur la base de données
	//$ligneE = mysqli_fetch_row($resultE);

	?>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Date</th>
            <th>Pression</th>
            <th>Température</th>
            <th>Hygrométrie</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th>Adresse</th>
        </tr>
        </thead>
        <tr>
			<?php
			while($ligneE=mysqli_fetch_row($resultE))   //Récupère une ligne de résultat sous forme de tableau indexé
			{
			while($ligneD=mysqli_fetch_row($resultD)) //double boucle
			{
			?>
            <td><?php echo("$ligneE[0]");?></td>
            <td><?php echo("$ligneD[3]");?></td>
            <td><?php echo("$ligneD[0] hPa");?></td>
            <td><?php echo("$ligneD[1]°C");?></td>
            <td><?php echo("$ligneD[2] %");?></td>
            <td><?php echo("$ligneE[1]°");?></td>
            <td><?php echo("$ligneE[2]°");?></td>
            <td><?php echo("$ligneE[3]");?></td>

        </tr>
		<?php } } ?>
    </table>

	<?php

	/* Libération des résultats */
	mysqli_free_result($result);

	/* Fermeture de la connexion */
	mysqli_close($connex);
	?>


	<?php /*
			try
		{
			// On se connecte à MySQL
			$bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '',
			array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));  //pour activer les erreurs de façon + claire
		}
		
		catch(Exception $e)
		{
			// En cas d'erreur, on affiche un message et on arrête tout
				die('Erreur : '.$e->getMessage());
		}
		
		$reponse = $mysqli->query("SELECT pression FROM mesures");
*/
	?>

	<?php
	/*
	while($donnees = mysqli_fetch_array($reponse))
	{
		?>
			<tr>
				<th><?php echo $donnees['pression'];?></th>
			</tr>
	<?php
	} //fin de la boucle, le tableau contient toute la BDD
	?>

		/* Libération du jeu de résultats */
	/*   mysqli_free_result($result);
*/

	/*
	//$result = $bdd->query("SELECT pression FROM mesures");
	$result = mysql_query("SELECT pression FROM mesures");

	echo $result;

	while ($row = mysql_fetch_array($result, MYSQL_BOTH)) {
		echo($row["pression"]);
	}
	*/

	/*
	$result = $bdd->query("SELECT pression FROM mesures");

	while ($row = mysql_fetch_array($result, MYSQL_BOTH)) {
	printf("ID : %s", $row['pression']);
	}
	*/

	/*
	$reponse = $bdd->query('SELECT * FROM mesures');

	while($row = $req->fetch()) { ?>


	<td><? //echo $row['pression']; ?></td>
	<td><? //echo $row['hygrométrie']; ?></td>

<?php
while ($ligne = mysql_fetch_row($resultat))
{
fputs($r, "$ligne[0]");fputs($fp, ";");
fputs($fp, "$ligne[1]");fputs($fp, ";");
fputs($fp, "\n");
}*/

	echo "OK";
	exit();
	?>

</table>
